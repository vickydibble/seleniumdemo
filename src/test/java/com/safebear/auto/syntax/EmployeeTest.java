package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {

    @Test
    public void employeeTest() {

        Employee hannah = new Employee();

        hannah.employ();
        hannah.setEmployed(false);

        System.out.println("Hannah's employment status = " + hannah.getEmployed());
    }



}
