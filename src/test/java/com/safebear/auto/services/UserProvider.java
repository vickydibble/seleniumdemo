package com.safebear.auto.services;

import com.safebear.auto.models.User;

import java.net.UnknownServiceException;
import java.util.HashMap;

public class UserProvider {
    public HashMap users;

    public UserProvider(){
        this.users = new HashMap<String, User>();
        this.users.put("invalidUser", new User("Hacker","dontletmein"));
        this.users.put("validUser", new User("tester","letmein"));
    }

    public User getUser(String userIdentifier){
        return (User) this.users.get(userIdentifier);
    }
}
